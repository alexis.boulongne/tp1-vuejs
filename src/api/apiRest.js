import Vue from 'vue'
const url = 'http://163.172.53.19:8081/api/beers'

export default class ApiRest {
  static getBeers (owner, size) {
    return Vue.http.get(`${url}?owner=${owner}&size=${size}`, {})
  }

  static getBeerTypes () {
    return Vue.http.get(`${url}/types`, {})
  }

  static getBeer (owner, id) {
    return Vue.http.get(`${url}/${id}?owner=${owner}`, {})
  }

  static addBeer (owner, beer) {
    return Vue.http.post(`${url}?owner=${owner}`, beer)
  }

  static updateBeer (beer) {
    return Vue.http.put(`${url}/${beer.id}?owner=${beer.owner}`, beer)
  }

  static deleteBeer (owner, id) {
    return Vue.http.delete(`${url}/${id}?owner=${owner}`, {})
  }
}
