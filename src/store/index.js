import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: 'Alexis',
    selectedBeer: null
  },
  mutations: {
    selectBeer (state, beer) {
      state.selectedBeer = beer
    },
    resetSelectedBeer (state) {
      state.selectedBeer = null
    }
  },
  getters: {
    getSelectedBeer (state) {
      return state.selectedBeer
    },
    getUser (state) {
      return state.user
    }
  }
})
