import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import Toasted from 'vue-toasted'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faBeer, faTrashAlt, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret, faBeer, faTrashAlt, faTimes)
dom.watch()
Vue.component('font-awesome-icon', FontAwesomeIcon)
/* doc sur : https://github.com/FortAwesome/vue-fontawesome#get-started */
Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(Toasted, {
  duration: 3000,
  position: 'top-right',
  theme: 'bubble',
  iconPack: 'fontawesome'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
