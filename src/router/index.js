import Vue from 'vue'
import VueRouter from 'vue-router'
import BeerForm from '@/components/BeerForm.vue'
import BeerArray from '@/components/BeerArray.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'BeerArray',
    component: BeerArray
  },
  {
    path: '/form',
    name: 'BeerForm',
    component: BeerForm
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
